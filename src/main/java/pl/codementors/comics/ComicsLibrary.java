package pl.codementors.comics;


import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     *
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {
        return comics;
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (comic != null) {
            comics.add(comic);
        }
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        comics.remove(comic);
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {
        for (Comic comic : comics) {
            comic.setCover(cover);
        }
    }
    /**
     *
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Collection<String> authors = new HashSet<>();
        for (Comic comic : comics) {
            authors.add(comic.getAuthor());
        }
        return authors;
    }
    public Collection<Comic> getComicsByAuthor(String author) {

        HashSet<Comic> comicsOfAuthor = new HashSet<>();

        for (Comic comic : comics) {
            if (comic.getAuthor().equals(author)) {
                comicsOfAuthor.add(comic);
            }
        }
        return comicsOfAuthor;
    }

    /**
     *
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {
        Collection <String> series = new HashSet<>();
        for (Comic comic : comics) {
            series.add(comic.getSeries());
        }
        return series;
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     *
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     *
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     *
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {
        try (FileReader fr = new FileReader(file);
             Scanner scanner = new Scanner(fr)) {
            int numberOfComics = scanner.nextInt();
            scanner.skip("\n");
            for (int i = 0; i < numberOfComics; i++) {

                Comic comic = new Comic();
                String title = scanner.nextLine();
                String author = scanner.nextLine();
                String series = scanner.nextLine();
                String sCover = scanner.next();
                int publishMonth = scanner.nextInt();
                int publishYear = scanner.nextInt();
                scanner.skip("\n");

                comic.setTitle(title);
                comic.setAuthor(author);
                comic.setSeries(series);
                comic.setCover(Comic.Cover.valueOf(sCover));
                comic.setPublishMonth(publishMonth);
                comic.setPublishYear(publishYear);


                /*
                Comic comic = new Comic(scanner.nextLine(),scanner.nextLine(),
                scanner.nextLine(),Comic.Cover.valueOf(scanner.next()),scanner.nextInt(),
                scanner.nextInt());
                scanner.skip("\n");
                */
                comics.add(comic);
            }

        } catch (RuntimeException | IOException ex) {
            System.out.println("Exception!, do nothing");
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {
        int counter = 0;
        for (Comic comic : comics) {
            if (comic.getSeries().equals(series)) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {
        int counter = 0;
        for (Comic comic : comics) {
            if (comic.getAuthor().equals(author)) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int counter = 0;
        for (Comic comic : comics) {
            if (comic.getPublishYear() == year) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {
        int counter = 0;
        for (Comic comic : comics) {
            if ((comic.getPublishYear() == year) && (comic.getPublishMonth() == month)) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {
        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if (comic.getPublishYear() < year) {
                iterator.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if (comic.getAuthor().equals(author)) {
                iterator.remove();
            }
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {

        Map<String, Collection<Comic>> comicsByAuthor = new HashMap<>();

        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if (comicsByAuthor.containsKey(comic.getAuthor())) {
                comicsByAuthor.get(comic.getAuthor()).add(comic);
            }
            else {
                HashSet<Comic> comicsFromAuthor = new HashSet<>();
                comicsFromAuthor.add(comic);
                comicsByAuthor.put(comic.getAuthor(), comicsFromAuthor);
            }
        }

        /*
        for (Comic comic : comics) {
            if (comicsByAuthor.containsKey(comic.getAuthor())) {
                comicsByAuthor.get(comic.getAuthor()).add(comic);
            }
            else {
                HashSet<Comic> comicsFromAuthor = new HashSet<>();
                comicsFromAuthor.add(comic);
                comicsByAuthor.put(comic.getAuthor(), comicsFromAuthor);
            }
        }

        Map<String, Set<Comic>> groupByAuthor =
                comics.stream().collect(Collectors.groupingBy(Comic::getAuthor));
        return groupByAuthor;
        */
        return comicsByAuthor;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {

        Map<Integer, Collection<Comic>> comicsByPublishYear = new HashMap<>();

        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if (comicsByPublishYear.containsKey(comic.getPublishYear())) {
                comicsByPublishYear.get(comic.getPublishYear()).add(comic);
            }
            else {
                HashSet<Comic> comicsFromPublishYear = new HashSet<>();
                comicsFromPublishYear.add(comic);
                comicsByPublishYear.put(comic.getPublishYear(), comicsFromPublishYear);
            }
        }
        return comicsByPublishYear;
    }

    public void removeAllNewerThan(int year){

        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            if (comic.getPublishYear() > year) {
                iterator.remove();
            }
        }
    }

    public void removeAllFromYearAndMonth(int i, int i1) {
    }
    public Map<Pair<Integer,Integer>,Collection<Comic>>getYearsMonthsComics() {

        Map<Pair<Integer, Integer>, Collection<Comic>> yearsMonthsComics = new HashMap<>();

        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            Pair<Integer, Integer> pair = new ImmutablePair<>(comic.getPublishYear(), comic.getPublishMonth());
            if (yearsMonthsComics.containsKey(pair)) {
                yearsMonthsComics.get(pair).add(comic);
            } else {
                HashSet<Comic> comicsFromPublishYearAndMonths = new HashSet<>();
                comicsFromPublishYearAndMonths.add(comic);
                yearsMonthsComics.put(pair, comicsFromPublishYearAndMonths);
            }
        }
        return yearsMonthsComics;
    }
}